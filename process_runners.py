import os

from lib import pipe
from lib import identity
from tokenizer import detokenize
from tokenizer import token_generator


def preprocessor(text, app_state, seq):
    return text, app_state


def postprocessor(text, app_state, seq):
    return pipe(seq or [], identity, text, app_state)


def processor(text, app_state, seq):
    app_state['tag_path'] = app_state.get('tag_path', [])
    parsed = []

    seq = seq or []
    for token in token_generator(text):
        parsed_chunk, app_state = pipe(seq, detokenize, token, app_state)
        parsed.extend(parsed_chunk)

    return ''.join(parsed), app_state


def process_text(text, pre_seq=None, proc_seq=None, post_seq=None, **kwargs):
    app_state = kwargs

    pre, app_state = preprocessor(text, app_state, pre_seq)
    proc, app_state = processor(pre, app_state, proc_seq)
    post, app_state = postprocessor(proc, app_state, post_seq)

    return post, app_state


def process_file(filename,
                 pre_seq=None, proc_seq=None, post_seq=None, **kwargs):
    with file(filename) as f:
        input_content = f.read()
        html = input_content

    path = os.path.dirname(filename)
    filename_no_ext, ext = os.path.splitext(os.path.basename(filename))
    converted_filename = os.path.join(path, filename_no_ext + '_ok' + ext)
    debug_filename = os.path.realpath(os.path.join(
        os.path.curdir, 'fails', filename_no_ext + '_debug.txt'))

    result, app_state = process_text(html,
                                     pre_seq=pre_seq,
                                     proc_seq=proc_seq,
                                     post_seq=post_seq,
                                     **kwargs)
    suc = not len(app_state['tag_path'])

    with open(converted_filename, 'w') as o:
        o.write(result)

    if suc:
        os.remove(filename)
        print "File '%s' converted to '%s'" % (
            filename, converted_filename)
    else:
        print "Couldn't convert '%s'" % filename
        os.rename(
            os.path.realpath(filename),
            os.path.realpath(os.path.join(
                os.path.curdir,
                'fails',
                os.path.basename(filename))))
        with open(debug_filename, 'w') as debug:
            debug.write(repr(app_state['tag_path']))
