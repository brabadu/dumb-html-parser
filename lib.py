def identity(*i):
    return i


def bind(a, b):
    return lambda *args: b(a, *args)


def pipe(seq, fresult, *args):
    return reduce(bind, reversed(seq), fresult)(*args)
