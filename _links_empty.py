import sys

from settings import H1_CONTENT
from settings import PLACEHOLDER_TEXT
from settings import BIGPLACEHOLDER_TEXT
from processors import h1_processor
from processors import link_processor
from processors import h1_postprocessor
from processors import placeholder_processor
from processors import placeholder_empty_postprocessor
from processors import title_postprocessor
from processors import rep_postprocessor
from process_runners import process_file


if __name__ == '__main__':
    files = sys.argv[1:]
    if not files:
        print 'No file was provided for conversion'
        exit(-1)

    links_count = sys.argv[2:]
    if not links_count:
        print 'Set number of links to change'
        exit(-1)

    placeholder_key = sys.argv[3:]
    if not len(placeholder_key):
        print 'Set keyword for placeholder'
        exit(-1)

    placeholder_key = placeholder_key[0]
    if (
        not placeholder_key in H1_CONTENT
        or not placeholder_key in PLACEHOLDER_TEXT
        or not placeholder_key in BIGPLACEHOLDER_TEXT
    ):
        print (
            "Theres no keyword '%s' in H1_CONTENT "
            "or PLACEHOLDER_TEXT or BIGPLACEHOLDER_TEXT" % placeholder_key)
        exit(-1)

    reps = sys.argv[4:]

    links_count = int(links_count[0])
    process_file(files[0],
                 proc_seq=[placeholder_processor,
                           link_processor,
                           h1_processor],
                 post_seq=[placeholder_empty_postprocessor,
                           h1_postprocessor,
                           title_postprocessor,
                           rep_postprocessor],
                 links_count=links_count,
                 placeholder_key=placeholder_key,
                 reps=reps)
