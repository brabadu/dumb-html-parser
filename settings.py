# would become [LINK] and [/LINK]
LINK_OPEN = '[LINK]'
LINK_CLOSE = '[/LINK]'
LINK_HREF = '[URL]'
LINK_TITLE = '[ANCHOR]'
LINK_ALT = '[ANCHOR]'
LINK_CONTENT = '[ANCHOR]'

H1_CONTENT = {
    'qwert': '[H1]',
}

PLACEHOLDER_TEXT = {
    'qwert': '[TEXT]',
}

BIGPLACEHOLDER_TEXT = {
    'qwert': '[BIGTEXT][REP5][REP1][REP5]',
}

TITLE_TEXT = {
    'qwert': '[TITLE]',
}
