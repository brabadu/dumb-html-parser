#!/usr/bin/python
import os
import shutil
import unittest

from lib import identity
from tokenizer import Token
from tokenizer import token_generator
from processors import link_processor
from processors import title_postprocessor
from processors import placeholder_processor
from processors import placeholder_longest_postprocessor
from processors import rep_postprocessor

from process_runners import process_file
from process_runners import process_text
from process_runners import processor


OK_HTML = """<html><head><script>j>>>3</script>
<style>head > div</style></head><body>
<!--comment >><div></div>-->
<div>Lorem ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum
<p>Some other text loremloremloremlorem</p>
loremloremloremloremloremlorem catapopum</div>
<i>Para banana corata</i>
</body></html>
"""
BROKEN_HTML = """</head>
<div>Lorem ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum
loremloremloremloremloremlorem catapopum</div></html>
"""
OK_HTML_RESULT = """<html><head><script>j>>>3</script>
<style>head > div</style></head><body>
<!--comment >><div></div>-->
<div>[TEXT]<p>[TEXT]</p>[TEXT]</div>
<i>Para banana corata</i>
</body></html>
"""


class TestParser(unittest.TestCase):
    def test_tag_generator(self):
        tagger = token_generator(OK_HTML)
        self.assertEquals(Token('', '<html>', 'html', False), tagger.next())
        self.assertEquals(Token('', """<head><script>j>>>3</script>
<style>head > div</style></head>""", 'head', False), tagger.next())
        self.assertEquals(Token('', '<body>', 'body', False), tagger.next())
        self.assertEquals(
            Token('\n', '<!--comment >><div></div>-->', '!--', False),
            tagger.next())
        self.assertEquals(Token('\n', '<div>', 'div', False), tagger.next())
        self.assertEquals(
            Token('Lorem ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum\n',
                  '<p>', 'p', False),
            tagger.next())
        self.assertEquals(
            Token('Some other text loremloremloremlorem', '</p>', 'p', True),
            tagger.next())
        self.assertEquals(
            Token('\nloremloremloremloremloremlorem catapopum',
                  '</div>', 'div', True),
            tagger.next())
        self.assertEquals(Token('\n', '<i>', 'i', False), tagger.next())
        self.assertEquals(Token('Para banana corata', '</i>', 'i', True),
                          tagger.next())
        self.assertEquals(Token('\n', '</body>', 'body', True), tagger.next())
        self.assertEquals(Token('', '</html>', 'html', True), tagger.next())
        self.assertEquals(Token('\n', '', '', False), tagger.next())
        self.assertRaises(StopIteration, tagger.next)

    def test_broken_layout_tag_generator(self):
        tagger = token_generator(BROKEN_HTML)
        self.assertEquals(Token('', '</head>', 'head', True), tagger.next())
        self.assertEquals(Token('\n', '<div>', 'div', False), tagger.next())
        self.assertEquals(Token("""Lorem ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum
loremloremloremloremloremlorem catapopum""", '</div>', 'div', True), tagger.next())
        self.assertEquals(Token('', '</html>', 'html', True), tagger.next())
        self.assertEquals(Token('\n', '', '', False), tagger.next())
        self.assertRaises(StopIteration, tagger.next)

    def test_processor_standard_page(self):
        result, app_stat = processor(
            OK_HTML,
            {'links_count': 1},
            [placeholder_processor, link_processor])
        self.assertEquals(OK_HTML, result)
        self.assertEquals(app_stat, {
            'links_count': 1,
            'tag_path': [],
            'placeholder_processor': {
                'list': ['Lorem ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum\n',
                         'Some other text loremloremloremlorem',
                         '\nloremloremloremloremloremlorem catapopum']},
        })

    def test_process_file(self):
        shutil.copy('fixtures/test_fixt.html', 'fixtures/test.html')
        process_file('fixtures/test.html',
                     proc_seq=[placeholder_processor, link_processor],
                     post_seq=[placeholder_longest_postprocessor],
                     placeholder_key='qwert',
                     links_count=1)

        must_be = file('fixtures/test_ok_prep.html').read()
        it_is = file('fixtures/test_ok.html').read()

        self.assertEquals(must_be, it_is)

        os.remove('fixtures/test_ok.html')

    def test_process_with_placeholder_processor(self):
        in_text = """<p>Lorem 111ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum<a>asdf</a>
Lorem 222ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum<a>asdf</a>
Lorem 333ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum</p>"""

        text, app_state = process_text(in_text,
                                       proc_seq=[placeholder_processor],
                                       post_seq=[placeholder_longest_postprocessor],
                                       placeholder_key='qwert')
        self.assertEquals(app_state, {
            'placeholder_key': 'qwert',
            'tag_path': [],
            'placeholder_processor': {'list': [
                'Lorem 111ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum',
                '\nLorem 222ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum',
                '\nLorem 333ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum',
            ]}})

        self.assertEquals(text, """<p>[BIGTEXT]<a>asdf</a>
Lorem 222ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum<a>asdf</a>
Lorem 333ipsum Ipsum IpsumIpsum IpsumIpsum Ipsum</p>""")

    def test_placeholder_processor_empty(self):
        t = Token('text', '<a href="asdf">', 'a', True)
        app_state = {'tag_path': []}
        res_t, res_app_state = placeholder_processor(identity, t, app_state)

        self.assertEquals(res_app_state, {
            'tag_path': [],
            'placeholder_processor': {'list': []}})

    def test_title_postprocessor(self):
        text = """
	<title>  A Broad Scot The International Scottish Culture News Magazine</title>
	"""
        app_state = {'placeholder_key': 'qwert'}
        res_text, res_app_state = title_postprocessor(identity, text, app_state)
        self.assertEqual(res_text, """
	<title>[TITLE]</title>
	""")
        self.assertEqual(res_app_state, app_state)

    def test_reps_postprocessor(self):
        text = "Something [REP1] inside [REP100] me"
        app_state = {'reps': ['more']}
        res_text, res_app_state = rep_postprocessor(identity, text, app_state)
        self.assertEqual(res_text, "Something more inside [REP100] me")

if __name__ == '__main__':
    unittest.main()
