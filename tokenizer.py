from collections import namedtuple

useful_tags = ['div', 'p', 'span']
skipping_tags = ['script', 'head', 'style', '!--']
selfclosing_tags = lambda tag: tag in [
    '!doctype', 'br', 'img', 'input', 'label', 'link', 'hr', 'param', 'col',
    'area', 'meta', 'frame', 'embed', 'wbr'
] + skipping_tags


Token = namedtuple('Token',
                   ['pre_tag', 'tag_content', 'tag', 'closing'],
                   verbose=False)


def detokenize(token, state):
    return [token.pre_tag, token.tag_content], state


def token_generator(text):
    pre_tag, opening_bracket, rest = text.partition('<')

    while opening_bracket:
        tag_content, closing_bracket, rest = rest.partition('>')

        # tag, maybe with closing / in front
        norm_tag = tag_content.strip().split()[0].lower()

        # tag, guaranteed to be without /
        tag_name = norm_tag.strip('//')

        closing = norm_tag.startswith('/')

        # it is comment
        if tag_name.startswith('!--'):
            rest = tag_content + closing_bracket + rest
            tag_content, closing_bracket, rest = rest.partition('-->')
            tag_name = '!--'

        # it is tag to skip
        elif tag_name in skipping_tags and not closing:
            rest = tag_content + closing_bracket + rest
            (tag_content,
             closing_bracket,
             rest) = rest.partition('</%s>' % tag_name)

        yield Token(pre_tag,
                    opening_bracket + tag_content + closing_bracket,
                    tag_name,
                    closing)
        pre_tag, opening_bracket, rest = rest.partition('<')

    yield Token(pre_tag, '', '', False)
