import re

import settings
from tokenizer import useful_tags
from tokenizer import selfclosing_tags
from tokenizer import detokenize


TEXT_THRESHOLD = 20


def h1_processor(cb, token, app_state):
    # first h1 already processed, no more needed
    if app_state.get('h1_processed'):
        return cb(token, app_state)

    # remove other tags inside h1
    if not token.tag == 'h1' and 'h1' in app_state['tag_path']:
        return detokenize(token._replace(pre_tag='', tag_content=''),
                          app_state)

    # put h1 right after body if there's no in text
    if token.tag == 'body' and not token.closing:
        return cb(
            token._replace(
                tag_content=(token.tag_content +
                             "<h1>%s</h1>" % settings.H1_CONTENT[app_state['placeholder_key']])),
            app_state)

    # mark h1 as processed on closing tag if there's one
    if token.tag == 'h1' and token.closing:
        app_state['h1_processed'] = True
        return cb(token._replace(pre_tag=settings.H1_CONTENT[app_state['placeholder_key']]),
                  app_state)

    return cb(token, app_state)


def link_processor(cb, token, app_state):
    # if not Link or Limit exceeded
    if (not token.tag == 'a' or not app_state.get('links_count')):
        return cb(token, app_state)

    # if there's no href in link
    if (
        not token.closing and token.tag_content.lower().find('href') == -1
    ):
        app_state['link_without_href'] = True
        return cb(token, app_state)

    # handling closing tag if there's no href in opening one
    if (
        token.closing and app_state.get('link_without_href')
    ):
        app_state.pop('link_without_href')
        return cb(token, app_state)

    pre_tag = token.pre_tag
    tag_content = token.tag_content
    if token.closing:
        tag_content += settings.LINK_CLOSE
        pre_tag = settings.LINK_CONTENT
        app_state['links_count'] -= 1

    else:
        pre_tag += '%s' % settings.LINK_OPEN
        tag_content = re.sub(
            r'href=(\'.*?\'|".*?")',
            'href="%s"' % settings.LINK_HREF,
            tag_content)
        tag_content = re.sub(
            r'title=(\'.*?\'|".*?")',
            'title="%s"' % settings.LINK_TITLE,
            tag_content)
        tag_content = re.sub(
            r'alt=(\'.*?\'|".*?")',
            'alt="%s"' % settings.LINK_ALT,
            tag_content)

    return cb(token._replace(pre_tag=pre_tag, tag_content=tag_content),
              app_state)


def placeholder_processor(cb, token, app_state):
    state = app_state.get('placeholder_processor', {
        'list': [],
    })

    # is closing tag? and if the last tag we're in
    if token.closing:
        if (
            app_state['tag_path'] and
            token.tag == app_state['tag_path'][-1]
        ):
            app_state['tag_path'].pop()

        if token.tag in useful_tags:
            state['list'].append(token.pre_tag)

    # opening tag
    else:
        if (
            len(app_state['tag_path'])
            and app_state['tag_path'][-1] in useful_tags
        ):
            state['list'].append(token.pre_tag)

        if token.tag and not selfclosing_tags(token.tag):
            app_state['tag_path'].append(token.tag)

    app_state['placeholder_processor'] = state
    return cb(token, app_state)


def placeholder_longest_postprocessor(cb, text, app_state):
    list_ = app_state.get(
        'placeholder_processor', {}).get('list', [])

    to_change = sorted(filter(lambda x: len(x) >= TEXT_THRESHOLD,
                              map(str.strip, list_)),
                       key=len,
                       reverse=True)

    if to_change:
        longest, to_change = to_change[0], to_change[1:]

        text = text.replace(
            longest,
            settings.BIGPLACEHOLDER_TEXT[app_state['placeholder_key']],
            1)

    return cb(text, app_state)


def placeholder_empty_postprocessor(cb, text, app_state):
    list_ = app_state.get(
        'placeholder_processor', {}).get('list', [])

    to_change = sorted(filter(lambda x: len(x) >= TEXT_THRESHOLD,
                              map(str.strip, list_)),
                       key=len,
                       reverse=True)

    if to_change:
        longest, rest_to_change = to_change[0], to_change[1:]

        text = text.replace(
            longest,
            settings.BIGPLACEHOLDER_TEXT[app_state['placeholder_key']],
            1)

        for item in rest_to_change:
            text = text.replace(
                item,
                '',
                1)

    return cb(text, app_state)


def h1_postprocessor(cb, text, app_state):
    return cb((text.replace('<body><h1>%s</h1>' % settings.H1_CONTENT[app_state['placeholder_key']], '<body>', 1)
               if app_state.get('h1_processed') else text),
              app_state)


def title_postprocessor(cb, text, app_state):
    title_re = r'(.*<title>).*(</title>.*)'
    placeholder = '\\1{}\\2'.format(settings.TITLE_TEXT[app_state['placeholder_key']])

    match = re.match(title_re, text, re.I + re.S)

    changed_title = match.expand(placeholder) if match else text
    return cb(changed_title, app_state)


def rep_postprocessor(cb, text, app_state):
    reps = app_state.get('reps', [])

    repped = text
    for i in re.finditer(r'\[REP(\d+)\]', text):
        rep_num = int(i.group(1))
        if rep_num <= len(reps):
            repped = repped.replace(i.group(), reps[rep_num - 1])

    return cb(repped, app_state)
